# findent_batch

`findent_batch` is a POSIX shell script for running [findent](https://sourceforge.net/projects/findent/) recursively on .F90/.f90 files in a given path. This script was originally developed for a fork of findent called [findent-octopus](https://gitlab.com/octopus-code/findent-octopus).

## requirements

- findent
- git
- diff, which, tail, wc

## usage

- see help

```
findent_batch --help
```

- run `findent` on all the .F90/.f90 files in `./tests` and fix indentations

```
findent_batch --dir=./tests
```

- run `findent` on all the .F90/.f90 files but do not fix indentations. Fail if indentation is not correct. generate a patch file if the test path is a git repository.

```
findent_batch --dry-run
```

- generate patch file in `/new/path`

```
findent_batch --patch-dir=/new/path --dry-run 
```

- use `/path/to/findent_new` instead of `findent`

```
findent_batch --findent-exec=/path/to/findent_new
```

- pass `-i4 -c4` flags to `findent` (default flags: `-i2 -c2`)

```
findent_batch --findent-flags="-i4 -c4"
```

- run without any output logs

```
findent_batch --silent
```

## development

- run tests

```
make -C tests
```
